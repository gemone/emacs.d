;;; init.el --- Load the full configuration -*- lexical-binding: t -*-
;;; Commentary:

;; This file bootstraps the configuration, which is divided into
;; a number of other files.

;;; Code:

;; Minimal Version: Just test in v26.3
(let* ((minver "26.3"))
  (when (version< emacs-version minver)
    (error "Emacs v%s or higher is required." minver)))

;; set init file-path
(setq user-init-file (or load-file-name (buffer-file-name)))
(setq usjer-emacs-directory (file-name-directory user-init-file))

;; do not resize frame
(setq frame-inhibit-implied-resize t)
;; single mode
(setq initial-major-mode 'fundamental-mode)
(setq package-enable-at-startup nil
      package--init-file-ensured t)

;; garbage collection, and thereafter
;; 定义相关变量
(defvar normal-gc-cons-threshold (* 20 1024 1024))
(defvar better-gc-cons-threshold (* 64 1024 2024))
(defvar init-gc-cons-threshold (* 128 1024 1024))

;; 垃圾自动回收
(add-hook 'emacs-startup-hook
	  (lambda ()
	    ;; --- Better GC start ---
	    (setq gc-cons-threshold normal-gc-cons-threshold)
	    ;; --- Better GC end ---

	    ;; --- Auto GC start ---
	    (if (boundp 'after-focus-change-function)
		(add-function :after after-focus-change-function
			      (lambda ()
				(unless (frame-focus-state)
				  (garbage-collect))))
	      (add-hook 'after-focus-change-function 'garbage-collect))

	    (defun gc-minibuffer-setup-hook ()
	      (setq gc-cons-threshold init-gc-cons-threshold))

	    (defun gc-minibuffer-exit-hook ()
	      (garbage-collect)
	      (setq gc-cons-threshold better-gc-cons-threshold))

	    (setq read-process-output-max (* 1024 1024)) ;; 1mb
	    (add-hook 'minibuffer-setup-hook #'gc-minibuffer-setup-hook)
	    (add-hook 'minibuffer-exit-hook #'gc-minibuffer-exit-hook)
 	    ;; --- Auto GC end ---
	    ))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file)

;; load the file without message
(with-temp-message ""
  (org-babel-load-file (expand-file-name "~/.emacs.d/README.org"))
  )

;;; init.el ends here
