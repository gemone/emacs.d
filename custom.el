(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("24714e2cb4a9d6ec1335de295966906474fdb668429549416ed8636196cb1441" default))
 '(package-selected-packages
   '(markdown-toc grip-mode markdown-mode lua-mode ivy-xref company-tabnine vterm powerline moe-theme diminish use-package-ensure-system-package awesome-tab dired-x dired-aux all-the-icons-dired diredfl dired-rsync dired-git-info dired auto-save dracula-theme doom-modeline awesome-tray which-key-posframe company-posframe ivy-posframe rainbow-delimiters lsp-treemacs eaf web-mode company-lsp expand-region hungry-delete org-superstar org-superstar-mode yasnippet winum treemacs eglot nox ccls flycheck dap-mode lsp-ivy lsp-ui lsp-mode company company-mode symbol-overlay magit smartparans counsel snails use-package-ensure smartparens smartparens-config which-key htmlize emacs-htmlize rime posframe popup emacs-rime color-identifiers-mode dash all-the-icons quelpa-use-package quelpa git-package exec-path-from-shell try use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))
