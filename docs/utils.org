
#+TITLE: Utils 工具集合

此文件优先与其他所有文件，必须有限加载才能使用。

本文件主要包含一些有用的函数以及包管理器的配置，方便后面的配置使用。

* 包管理器

** 配置软件源

#+BEGIN_SRC emacs-lisp
  (require 'package)
  ;; Use nil package-archives
  (setq package-archives nil)
  ;; use package-user-directory
  (defconst package-user-dir (concat custom/emacs-d-elpa "emacs-" emacs-version))
  (let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		      (not (gnutls-available-p))))
	 (proto (if no-ssl "http" "https")))
    ;; GNU
    (add-to-list 'package-archives
		 (cons "gnu"
		       (concat proto "://elpa.emacs-china.org/gnu/")) t)
    ;; Melpa
    (add-to-list 'package-archives
		 (cons "melpa"
		       (concat proto "://elpa.emacs-china.org/melpa/")) t)
    ;; Org
    (add-to-list 'package-archives
		 (cons "org"
		       (concat proto "://elpa.emacs-china.org/org/")) t)
    )

  (setq package-enable-at-startup nil)
  (package-initialize)

  ;; update the package metadata is the local cache is missing
  (unless package-archive-contents
    (package-refresh-contents))
#+END_SRC

** use-package

- [[https://github.com/jwiegley/use-package][jwiegley/use-package]]

#+BEGIN_SRC emacs-lisp
  ;; Install `use-package'
  (unless (package-installed-p 'use-package)
    (package-install 'use-package))
#+END_SRC

** use-package expand

*** Quelpa:

 #+BEGIN_SRC emacs-lisp
   (defconst quelpa-melpa-repo-url custom/melpa-repo-url
     "The melpa git repository url.")

   (defconst quelpa-dir (concat custom/emacs-d-elpa "quelpa"))

   (defmacro utils/quelpa-git-https-sources--set (source-name source-url)
     "Add the source or modify default source."
     `(progn
	(defun ,(intern (format "quelpa-build--checkout-%s" source-name)) (name config dir)
	(let ((url (format "https://%s/%s.git" ,source-url (plist-get config :repo))))
	  (quelpa-build--checkout-git name (plist-put (copy-sequence config) :url url) dir)))))

   ;; quelpa for `use-package'
   (setq quelpa-self-upgrade-p nil)
   ;; (setq quelpa-upgrade-interval 7)
   ;; (add-hook #'after-init-hook #'quelpa-upgrade-all-maybe)

   (use-package quelpa
     :ensure t)

   (use-package quelpa-use-package
     :ensure t)

   ;; def
   (utils/quelpa-git-https-sources--set "gitee" "gitee.com")
   (utils/quelpa-git-https-sources--set "gmirror" custom/github-url)
 #+END_SRC

*** config use-package

#+BEGIN_SRC emacs-lisp
  ;; config `use-package'
  (use-package use-package
    :config
    (setq use-package-verbose 'error
	  use-package-expand-minimally t)

    ;; Add the keyword * no mean
    (defmacro utils/use-package-add-keywords (keyword)
      "Add the keyword"
      `(progn
	 (add-to-list 'use-package-keywords ,(intern (concat ":" keyword)) 'append)
	 (defun ,(intern (format "use-package-normalize/:%s" keyword)) (&rest _))
	 (defun ,(intern (format "use-package-handler/:%s" keyword)) (&rest _))
	 ))

    (utils/use-package-add-keywords "about")
    (utils/use-package-add-keywords "homepage")
    (utils/use-package-add-keywords "notes")
    (utils/use-package-add-keywords "info")
    )

  ;; Path
  (use-package exec-path-from-shell
    :if (memq window-system '(mac ns))
    :ensure t
    :config
    (exec-path-from-shell-initialize))

  ;; System-package <- keyword add
  (use-package use-package-ensure-system-package
    :ensure t)

  ;; diminish
  (use-package diminish
    :about Diminished modes are minor modes with no modeline display.
    :ensure t)

  ;; dash
  (use-package dash
    :ensure t
    :config
    ;; Will also make `it', `acc' into variable face everywhere
    (dash-enable-font-lock))
 #+END_SRC

* 有效工具

** try

#+BEGIN_SRC emacs-lisp
  (use-package try
    :notes Use it to try some package.
    :ensure t)
#+END_SRC
** 依赖

#+BEGIN_SRC emacs-lisp
  (use-package htmlize
    :homepage https://github.com/hniksic/emacs-htmlize
    :notes Use for org mode.
    :ensure t)

  (use-package popup
    :ensure t)

  (if (display-graphic-p)
      (use-package posframe
	:ensure t)
    )

  (use-package seq
    :ensure t)

#+END_SRC

* 自定义工具
* TODO 待办

** 处理=quelpa=的自动更新
